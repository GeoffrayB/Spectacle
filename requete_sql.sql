/* Insert spectacle' */
insert into spectacle
values('good by tour', 'johnny halliday', 'zenith de nantes', '2018-02-01', 200);
insert into spectacle
values('facebook', 'les vamps', 'zenith de paris', '2019-02-01', 500);

/* insert client */
insert into client
values('coignat', 'guillaume', 'guillaume.coignat@gmail.com', '79, rue de la gaudinière', '44100', 'Nantes');
insert into client
values('bigot', 'geoffray', 'geoffray.bigot@gmail.com', '1, rue de quelque part', '44000', 'Nantes');

/* Insert réservations */
insert into reservation
values('a1z2e3r4t5y6u7i8o9p0', 1, 1, 2, CURRENT_TIMESTAMP);
insert into reservation
values('0p9o2i3u4y5t6r7e8z9a', 2, 2, 4, CURRENT_TIMESTAMP);

/* By client */
select *
from reservation
where client_id = 1;

/* By Spectacle */
select *
from reservation
where spectacle_id = 1;

/* selectAll */
select r.code_reservation, r.nombre_places, r.date_reservation,
s.titre, s.artiste, s.lieu, s.date_spec, s.places_disponibles,
c.nom, c.prenom, c.email, c.adresse, c.code_postal, c.ville
from reservation r
join spectacle s on s.id = r.spectacle_id
join client c on c.id = r.client_id;

/* update reservation */
update reservation
set spectacle_id = ?, client_id = ?, nombre_places = ?, date_reservation = ?
where code_reservation = ?;