package fr.eni.spectacle.ihm;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.JobName;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.JTextField;

import fr.eni.spectacle.bll.BLLException;
import fr.eni.spectacle.bll.CatalogueManageur;
import fr.eni.spectacle.bo.Client;
import fr.eni.spectacle.bo.Reservations;
import fr.eni.spectacle.bo.Spectacle;

public class EcranSpectacle extends JFrame {
	
	// Panel
	int idSpectacle = (Integer) null;
	JLabel LabelId = new JLabel();
	
	JPanel panelListeSpectacles,
		   panelReservationsLabel,
		   panelReservationsTxt,
		   panelReservationsCExistant,
		   panelReservationsSucces,
		   panelListeClientsReservations,
		   panelListeClients;
	
	// Menu
	
	private JMenuBar menu_Principale = new JMenuBar();
	private JMenu menu_1 = new JMenu("Accueil");
	private JMenu menu_2 = new JMenu("R�servations");
	private JMenu menu_3 = new JMenu("Clients");
	
	private JLabel label_Titre = new JLabel();
	
	// Page Accueil - Liste des spectacles
	
	private JLabel label_RechercheSpectacle = new JLabel();
	private JTextField txt_RechercheSpectacle = new JTextField();
	private JButton button_RechercheSpectacle = new JButton();	
	
	private JLabel label_ArtisteTitreSpectacle = new JLabel(), label_LieuDateSpectacle = new JLabel();
	private JButton button_ReservationsSpectacle = new JButton();
	
	// Page R�servations 
	
	private JLabel label_ArtisteTitreReservation = new JLabel(), label_LieuDateReservation = new JLabel();
	private JLabel label_NbPlacesReservation = new JLabel();
	
		// Nouveau Client 
		
		private JLabel label_Nclient_NomReservation = new JLabel(), 
					   label_Nclient_PrenomReservation = new JLabel(),
					   label_Nclient_EmailReservation = new JLabel(),
					   label_Nclient_AdresseReservation = new JLabel(),
					   label_Nclient_CpReservation = new JLabel(),
					   label_Nclient_VilleReservation = new JLabel(),
					   label_Nclient_PlacesReservation = new JLabel();
	
		private JTextField txt_Nclient_NomReservation = new JTextField(), 
						   txt_Nclient_PrenomReservation = new JTextField(),
						   txt_Nclient_EmailReservation = new JTextField(),
						   txt_Nclient_AdresseReservation = new JTextField(),
						   txt_Nclient_CpReservation = new JTextField(),
						   txt_Nclient_VilleReservation = new JTextField(),
						   txt_Nclient_PlacesReservation = new JTextField();
		
		private JComboBox<Integer> cbox_Nclient_PlacesReservation = new JComboBox<Integer>();
		private JButton button_Nclient_ValiderReservation = new JButton();
		
		// Client existant 
					
		private JComboBox<String> cbox_Cexistant_EmailReservation = new JComboBox<String>();
		private JButton button_Cexistant_Valider = new JButton();
		
		// Dialog Erreur
		
		private JLabel label_Dialog_Erreur = new JLabel();
		private JLabel label_Dialog_ErreurChamps = new JLabel();
		private JLabel label_Dialog_ErreurPlaces = new JLabel();
		private JButton button_Dialog_Erreur = new JButton();
		
		// R�servation effectu�e 
		
		private JLabel label_SuccesReservation = new JLabel();
		private JLabel label_NumReservation = new JLabel();
		private JButton button_AccueilReservation = new JButton();
		
		// Liste des R�servations
		
		private JLabel label_Liste_NomReservation = new JLabel();
		private JLabel label_Liste_EmailReservation = new JLabel();
		private JLabel label_Liste_ArtisteTitreReservation = new JLabel();
		private JLabel label_Liste_DatePlacesReservation = new JLabel();
		private JButton button_Liste_AnnulerReservation = new JButton();
		
			// Dialog Annulation
			private JLabel label_Liste_Dialog_Annulation = new JLabel();
			private JLabel label_Liste_Dialog_Text = new JLabel();
			private JButton button_Liste_Dialog_Annulation = new JButton();
		
	
	// Clients
		
		private JLabel label_NomClient = new JLabel();
		private JLabel label_EmailClient = new JLabel();
		private JButton button_ReservationClient = new JButton();
		private JButton button_SupprimerClient = new JButton();
	
		
	

	public EcranSpectacle() throws BLLException {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
//		setLocationRelativeTo(null);
		setSize(800, 800);
		setResizable(true);
		setTitle("R�servation Spectacle");		
		menu_Principale.add(menu_1);
		menu_Principale.add(menu_2);
		menu_Principale.add(menu_3);
		setJMenuBar(menu_Principale);		
		ArrayList<Reservations> reser =  (ArrayList<Reservations>) new CatalogueManageur().getCatalogue();
		ArrayList<Spectacle> liste = new ArrayList<Spectacle>();
 		liste= (ArrayList<Spectacle>) new CatalogueManageur().getSpectacle();
 		initIhmAccueil(liste);
//		initIhmNewReservation();
//		initIhmListeReservation();
//		initIhmListeClient();
		setVisible(true);		
	}
	
	
	public void initIhmAccueil(ArrayList<Spectacle> spectacle) {

		JPanel panelPrincipal = new JPanel();
		panelPrincipal.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.insets = new Insets(5, 5, 5, 5);

		// Ligne 1
		gbc.gridy = 0;
		gbc.gridx = 1;		
		panelPrincipal.add(label_Titre, gbc);

		// Ligne 2
		gbc.gridy = 1;
		gbc.gridx = 0;
		panelPrincipal.add(new JLabel("Recherche par Artiste :"), gbc);		
		
		gbc.gridy = 1;
		gbc.gridx = 1;
		panelPrincipal.add(new JTextField(20), gbc);
		
		gbc.gridy = 1;
		gbc.gridx = 2;
		panelPrincipal.add(new JButton("ok"), gbc);
		
		int x = 2;
		for (int i = 0; i < spectacle.size(); i++) {			
			gbc.gridy = x;		
			gbc.gridx = 0;
			panelPrincipal.add(getPanelListeSpectacles(spectacle.get(i)), gbc);
			gbc.gridx = 4;			
			panelPrincipal.add(new JButton("R�servations"), gbc);
			gbc.gridx = 5;
			JLabel LabelId = new JLabel("ID "+ spectacle.get(i).getIdspectacle());
			LabelId.setVisible(true);
			panelPrincipal.add(LabelId,gbc);
			x++;
		}
		button_ReservationsSpectacle.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ApplicationController.actionAccueil(Integer.parseInt(((JLabel)button_ReservationsSpectacle.getParent().getComponent(2)).getText()));
				
			}
		});
		setContentPane(panelPrincipal);
	}
	
	
	public void initIhmNewReservation(ArrayList<Client> client) {

		JPanel panelPrincipal = new JPanel();
		panelPrincipal.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.insets = new Insets(5, 5, 5, 5);

		// Ligne 0
		gbc.gridy = 0;
		gbc.gridx = 1;		
		panelPrincipal.add(new JLabel("R�servation"), gbc);

		// Ligne 1
		gbc.gridy = 1;		
		gbc.gridx = 0;		
		panelPrincipal.add(new JLabel("Spectacle"), gbc);
		gbc.gridx = 4;
		panelPrincipal.add(new JLabel("Places disponibles :"), gbc);
		
		// Ligne 2
		gbc.gridy = 2;
		gbc.gridx = 0;
		panelPrincipal.add(new JLabel("- Nouveau Client -"), gbc);
		gbc.gridx = 4;
		panelPrincipal.add(new JLabel("- Client existant -"), gbc);
		
		// Ligne 3
		gbc.gridy = 3;
		gbc.gridx = 0;		
		panelPrincipal.add(getPanelLabelNClient(), gbc);
		
		gbc.gridx = 1;
		panelPrincipal.add(getPanelTxtNClient(), gbc);
		
		gbc.gridx = 4;
		panelPrincipal.add(getPanelCExistant(), gbc);
		
		//Ligne 4
		
		gbc.gridy = 4;
		gbc.gridx = 0;
		panelPrincipal.add(new JButton("Valider"), gbc);
		
		// Ligne 5
		
		gbc.gridy = 5;
		panelPrincipal.add(getPanelReservationsSucces(), gbc);
		
		setContentPane(panelPrincipal);
	}
	
	public void initIhmListeReservation() {

		JPanel panelPrincipal = new JPanel();
		panelPrincipal.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.insets = new Insets(5, 5, 5, 5);

		// Ligne 0
		gbc.gridy = 0;
		gbc.gridx = 1;		
		panelPrincipal.add(new JLabel("R�servations"));
		
		gbc.gridy = 1;		
		gbc.gridx = 0;		
		panelPrincipal.add(getPanelListeClientsReservations(), gbc);
		gbc.gridx = 4;
		panelPrincipal.add(new JButton("Annuler"), gbc);
		
		
		setContentPane(panelPrincipal);
	}
	
	public void initIhmListeClient() {

		JPanel panelPrincipal = new JPanel();
		panelPrincipal.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.insets = new Insets(5, 5, 5, 5);

		// Ligne 0
		gbc.gridy = 0;
		gbc.gridx = 1;		
		panelPrincipal.add(new JLabel("Clients"));
		
		gbc.gridy = 1;		
		gbc.gridx = 0;		
		panelPrincipal.add(getPanelListeClients(), gbc);
		gbc.gridx = 3;
		panelPrincipal.add(new JButton("R�servations"), gbc);
		gbc.gridx = 4;
		panelPrincipal.add(new JButton("Supprimer"), gbc);
		
		
		setContentPane(panelPrincipal);
	}
	 
	public JPanel getPanelListeSpectacles(Spectacle spectacle) {		
		panelListeSpectacles = new JPanel();
		panelListeSpectacles.setLayout(new BoxLayout(panelListeSpectacles, BoxLayout.Y_AXIS));		
		panelListeSpectacles.add(new JLabel(spectacle.getArtiste() + " - "+ spectacle.getTitre()));        
		panelListeSpectacles.add(new JLabel(spectacle.getLieu() + " / " + spectacle.getDate()));
		
		return panelListeSpectacles;
	}
	
	public JPanel getPanelLabelNClient() {
		if (panelReservationsLabel == null) {
			panelReservationsLabel = new JPanel();
			panelReservationsLabel.setLayout(new BoxLayout(panelReservationsLabel, BoxLayout.Y_AXIS));			
			panelReservationsLabel.add(new JLabel("Nom :"));	
			panelReservationsLabel.add(new JLabel("Prenom :"));
			panelReservationsLabel.add(new JLabel("Email :"));
			panelReservationsLabel.add(new JLabel("Adresse :"));
			panelReservationsLabel.add(new JLabel("Code postal :"));
			panelReservationsLabel.add(new JLabel("Ville :"));
			panelReservationsLabel.add(new JLabel("Places :"));		
		}

		return panelReservationsLabel;
	}
	
	public JPanel getPanelTxtNClient() {
		if (panelReservationsTxt == null) {
			panelReservationsTxt = new JPanel();
			panelReservationsTxt.setLayout(new BoxLayout(panelReservationsTxt, BoxLayout.Y_AXIS));			
			panelReservationsTxt.add(new JTextField(15));	
			panelReservationsTxt.add(new JTextField(15));	
			panelReservationsTxt.add(new JTextField(15));	
			panelReservationsTxt.add(new JTextField(15));
			panelReservationsTxt.add(new JTextField(15));	
			panelReservationsTxt.add(new JTextField(15));				
			panelReservationsTxt.add(new JComboBox<Integer>());				
		}

		return panelReservationsTxt;
	}
	
	public JPanel getPanelCExistant(){
		if (panelReservationsCExistant == null) {
			panelReservationsCExistant = new JPanel();
			panelReservationsCExistant.setLayout(new BoxLayout(panelReservationsCExistant, BoxLayout.Y_AXIS));				
			panelReservationsCExistant.add(new JComboBox<String>());
			panelReservationsCExistant.add(new JButton("Valider"));
		}

		return panelReservationsCExistant;
	}
	
	public JPanel getPanelReservationsSucces(){
		if (panelReservationsSucces == null) {
			panelReservationsSucces = new JPanel();
			panelReservationsSucces.setLayout(new BoxLayout(panelReservationsSucces, BoxLayout.Y_AXIS));				
			panelReservationsSucces.add(new JLabel("R�servation effectu�e !"));
			panelReservationsSucces.add(new JLabel("Num�ro de r�servation : 35X891VC"));
			panelReservationsSucces.add(new JButton("Accueil"));
		}

		return panelReservationsSucces;		
	}
	
	public JPanel getPanelListeClientsReservations() {
		if (panelListeClientsReservations == null) {
			panelListeClientsReservations = new JPanel();
			panelListeClientsReservations.setLayout(new BoxLayout(panelListeClientsReservations, BoxLayout.Y_AXIS));			
			panelListeClientsReservations.add(new JLabel("Micka�l VIAUD / mickael.viaud@gmail.com"));	
			panelListeClientsReservations.add(new JLabel("Johnny Hallyday, good bye tour / 2018-02-01 - 3 places"));			
		}

		return panelListeClientsReservations;
	}
	
	public JPanel getPanelListeClients() {
		if (panelListeClients == null) {
			panelListeClients = new JPanel();
			panelListeClients.setLayout(new BoxLayout(panelListeClients, BoxLayout.Y_AXIS));			
			panelListeClients.add(new JLabel("Micka�l VIAUD / mickael.viaud@gmail.com"));					
		}

		return panelListeClients;
	}

	public JLabel getLabel_Titre() {
		return label_Titre;
	}

	public JLabel getLabel_RechercheSpectacle() {
		return label_RechercheSpectacle;
	}

	public JTextField getTxt_RechercheSpectacle() {
		return txt_RechercheSpectacle;
	}

	public JButton getButton_RechercheSpectacle() {
		return button_RechercheSpectacle;
	}

	public JLabel getLabel_ArtisteTitreSpectacle() {
		return label_ArtisteTitreSpectacle;
	}

	public JLabel getLabel_LieuDateSpectacle() {
		return label_LieuDateSpectacle;
	}

	public JButton getButton_ReservationsSpectacle() {
		return button_ReservationsSpectacle;
	}

	public JLabel getLabel_ArtisteTitreReservation() {
		return label_ArtisteTitreReservation;
	}

	public JLabel getLabel_LieuDateReservation() {
		return label_LieuDateReservation;
	}

	public JLabel getLabel_NbPlacesReservation() {
		return label_NbPlacesReservation;
	}

	public JLabel getLabel_Nclient_NomReservation() {
		return label_Nclient_NomReservation;
	}

	public JLabel getLabel_Nclient_PrenomReservation() {
		return label_Nclient_PrenomReservation;
	}

	public JLabel getLabel_Nclient_EmailReservation() {
		return label_Nclient_EmailReservation;
	}

	public JLabel getLabel_Nclient_AdresseReservation() {
		return label_Nclient_AdresseReservation;
	}

	public JLabel getLabel_Nclient_CpReservation() {
		return label_Nclient_CpReservation;
	}

	public JLabel getLabel_Nclient_VilleReservation() {
		return label_Nclient_VilleReservation;
	}

	public JLabel getLabel_Nclient_PlacesReservation() {
		return label_Nclient_PlacesReservation;
	}

	public JTextField getTxt_Nclient_NomReservation() {
		return txt_Nclient_NomReservation;
	}

	public JTextField getTxt_Nclient_PrenomReservation() {
		return txt_Nclient_PrenomReservation;
	}

	public JTextField getTxt_Nclient_EmailReservation() {
		return txt_Nclient_EmailReservation;
	}

	public JTextField getTxt_Nclient_AdresseReservation() {
		return txt_Nclient_AdresseReservation;
	}

	public JTextField getTxt_Nclient_CpReservation() {
		return txt_Nclient_CpReservation;
	}

	public JTextField getTxt_Nclient_VilleReservation() {
		return txt_Nclient_VilleReservation;
	}

	public JTextField getTxt_Nclient_PlacesReservation() {
		return txt_Nclient_PlacesReservation;
	}

	public JComboBox<Integer> getCbox_Nclient_PlacesReservation() {
		return cbox_Nclient_PlacesReservation;
	}

	public JButton getButton_Nclient_ValiderReservation() {
		return button_Nclient_ValiderReservation;
	}

	public JComboBox<String> getCbox_Cexistant_EmailReservation() {
		return cbox_Cexistant_EmailReservation;
	}

	public JButton getButton_Cexistant_Valider() {
		return button_Cexistant_Valider;
	}

	public JLabel getLabel_Dialog_Erreur() {
		return label_Dialog_Erreur;
	}

	public JLabel getLabel_Dialog_ErreurChamps() {
		return label_Dialog_ErreurChamps;
	}

	public JLabel getLabel_Dialog_ErreurPlaces() {
		return label_Dialog_ErreurPlaces;
	}

	public JButton getButton_Dialog_Erreur() {
		return button_Dialog_Erreur;
	}

	public JLabel getLabel_SuccesReservation() {
		return label_SuccesReservation;
	}

	public JLabel getLabel_NumReservation() {
		return label_NumReservation;
	}

	public JButton getButton_AccueilReservation() {
		return button_AccueilReservation;
	}

	public JLabel getLabel_Liste_NomReservation() {
		return label_Liste_NomReservation;
	}

	public JLabel getLabel_Liste_EmailReservation() {
		return label_Liste_EmailReservation;
	}

	public JLabel getLabel_Liste_ArtisteTitreReservation() {
		return label_Liste_ArtisteTitreReservation;
	}

	public JLabel getLabel_Liste_DatePlacesReservation() {
		return label_Liste_DatePlacesReservation;
	}

	public JButton getButton_Liste_AnnulerReservation() {
		return button_Liste_AnnulerReservation;
	}

	public JLabel getLabel_Liste_Dialog_Annulation() {
		return label_Liste_Dialog_Annulation;
	}

	public JLabel getLabel_Liste_Dialog_Text() {
		return label_Liste_Dialog_Text;
	}

	public JButton getButton_Liste_Dialog_Annulation() {
		return button_Liste_Dialog_Annulation;
	}

	public JLabel getLabel_NomClient() {
		return label_NomClient;
	}

	public JLabel getLabel_EmailClient() {
		return label_EmailClient;
	}

	public JButton getButton_ReservationClient() {
		return button_ReservationClient;
	}

	public JButton getButton_SupprimerClient() {
		return button_SupprimerClient;
	}
	
}
