package fr.eni.spectacle.dal;

import java.util.List;

import fr.eni.spectacle.bo.*;


public interface SpectacleDAO {
	// Sélectionner une réservation par son code_reservation
	public Reservations selectById(String code_reservation) throws DALException;
	
	//Sélectionner toutes les réservations 
	public List<Reservations> selectAll() throws DALException;
	
	//Modifier les attributs d'une réservation connu en BD
	public void update(Reservations pReservations) throws DALException;
	
	//Insérer une nouvelle réservation
	public void insertReserv(Reservations pReservations) throws DALException;
	
	//Insérer une nouvelle réservation
	public void insertCli(Client pClient) throws DALException;
	
	//Insérer une nouvelle réservation
	public void insertSpec(Spectacle pSpectacle) throws DALException;
	
	//Supprimer une réservation
	public void delete(String code_reservation) throws DALException;
	
	//Sélectionner les réservations par client
	public List<Client> selectByClient() throws DALException;
	
	//Sélectionner les réservations par spectacle
	public List<Spectacle> selectBySpectacle() throws DALException;
	
	//Sélectionner les réservations par client
	public Client selectByIdClient(int id) throws DALException;
		
	//Sélectionner les réservations par spectacle
	public Spectacle selectByIdSpectacle(int id) throws DALException;
}
