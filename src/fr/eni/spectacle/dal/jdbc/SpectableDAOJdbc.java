package fr.eni.spectacle.dal.jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import fr.eni.spectacle.bo.Client;
import fr.eni.spectacle.bo.Reservations;
import fr.eni.spectacle.bo.Spectacle;
import fr.eni.spectacle.dal.DALException;
import fr.eni.spectacle.dal.SpectacleDAO;

public class SpectableDAOJdbc implements SpectacleDAO {

	private static final String sqlSelectById = "select code_reservation, nombre_places, date_reservation,"
			+ "s.titre as titreS, s.artiste as artisteS, s.lieu as lieuS, s.date_spec as dateSpec, s.places_disponibles as placesdispo ,"
			+ "c.nom as nomclient, c.prenom as prenomClient, c.email as emailClient, c.adresse as adresseClient, c.code_postal as codePostal, c.ville as villeClient from reservation "
			+ "join spectacle s on s.id = spectacle_id " + "join client c on c.id = client_id "
			+ "where code_reservation = ?;";

	private static final String sqlSelectAll = "select code_reservation, nombre_places, date_reservation,"
			+ "s.titre as titreS, s.artiste as artisteS, s.lieu as lieuS, s.date_spec as dateSpec, s.places_disponibles as placesdispo ,"
			+ "c.nom as nomclient, c.prenom as prenomClient, c.email as emailClient, c.adresse as adresseClient, c.code_postal as codePostal, c.ville as villeClient from reservation "
			+ "join spectacle s on s.id = spectacle_id " + "join client c on c.id = client_id ";

	private static final String sqlUpdate = "";
	
	private static final String sqlInsertReserv = "insert into reservation"
			+ "values(?,?, ?, ?, ?);";
	private static final String sqlInsertCli = "insert into client"
			+ "values(?, ?, ?, ?, ?, ?);";
	private static final String sqlInsertSpec = "insert into spectacle"
			+ "values(?, ?, ?, ?, ?);";
	private static final String sqlDelete = "Delete from reservation where code_reservation=?;";
	
	private static final String sqlSelectByclient = "select id as idClient,"
			+ "nom as nomclient, prenom as prenomClient, email as emailClient, adresse as adresseClient, code_postal as codePostal, ville as villeClient from client ";
	
	private static final String sqlSelectByspectacle = "select id as idspectacle,"
			+ "titre as titreS, artiste as artisteS, lieu as lieuS, date_spec as dateSpec, places_disponibles as placesdispo  from spectacle ";
	
	private static final String sqlSelectByIdclient = "select id as idClient,"
			+ "nom as nomclient, prenom as prenomClient, email as emailClient, adresse as adresseClient, code_postal as codePostal, ville as villeClient from client where id = ?";
	
	private static final String sqlSelectByIdspectacle = "select id as idspectacle,"
			+ "titre as titreS, artiste as artisteS, lieu as lieuS, date_spec as dateSpec, places_disponibles as placesdispo  from spectacle where id = ?";

	@Override
	public Reservations selectById(String code_reservation) throws DALException {
		Connection connect = null;
		PreparedStatement rqt = null;
		ResultSet result = null;
		Reservations res = null;

		try {
			connect = JdbcTools.getConnection();
			rqt = connect.prepareStatement(sqlSelectById);
			rqt.setString(1, code_reservation);
			result = rqt.executeQuery();
			if (result.next()) {
				res = new Reservations(new Spectacle(result.getString("titreS"), result.getString("artisteS"),
								result.getString("lieuS"), result.getDate("dateSpec"), result.getInt("placesDispo")),
						new Client(result.getString("nomClient"), result.getString("prenomClient"),
								result.getString("emailClient"), result.getString("adresseClient"),
								result.getString("codePostal"), result.getString("villeClient")),
						result.getInt("nombre_places"), result.getDate("date_reservation"));
			}
		} catch (SQLException e) {
			throw new DALException("selectById failed - Code reservation = " + code_reservation, e);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (rqt != null) {
					rqt.close();
				}
				if (connect != null) {
					connect.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			return res;
		
	}

	@Override
	public List<Reservations> selectAll() throws DALException {
		Connection connect = null;
		PreparedStatement rqt = null;
		ResultSet result = null;
		Reservations res = null;
		List<Reservations> liste = new ArrayList<Reservations>();
		try {
			connect = JdbcTools.getConnection();
			rqt = connect.prepareStatement(sqlSelectAll);
			result = rqt.executeQuery();

			while (result.next()) {
				res = new Reservations(new Spectacle(result.getString("titreS"), result.getString("artisteS"),
								result.getString("lieuS"), result.getDate("dateSpec"), result.getInt("placesDispo")),
						new Client(result.getString("nomClient"), result.getString("prenomClient"),
								result.getString("emailClient"), result.getString("adresseClient"),
								result.getString("codePostal"), result.getString("villeClient")),
						result.getInt("nombre_places"), result.getDate("date_reservation"));
				liste.add(res);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (rqt != null) {
					rqt.close();
				}
				if (connect != null) {
					connect.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			return liste;
		
	}

	@Override
	public void update(Reservations pReservations) throws DALException {
		// TODO Auto-generated method stub

	}

	@Override
	public void insertReserv(Reservations pReservations) throws DALException {
		Connection connect = null;
		PreparedStatement rqt = null;
		try {
			connect = JdbcTools.getConnection();
			rqt = connect .prepareStatement(sqlInsertReserv );
			rqt.setString(1, pReservations.generatecodeReservation());
			rqt.setInt(2, pReservations.getSpec().getIdspectacle());
			rqt.setInt(3, pReservations.getClient().getIdClient());
			rqt.setInt(4, pReservations.getNb_places());
			rqt.setDate(5, (Date) pReservations.getDateReservation());
			
		} catch (SQLException e) {
			throw new DALException("Insert Reservation failed - " +pReservations.getCodeReservation(), e);
		}
	}
	
	@Override
	public void insertCli(Client pClient) throws DALException {
		Connection connect = null;
		PreparedStatement rqt = null;
		try {
			connect = JdbcTools.getConnection();
			rqt = connect .prepareStatement(sqlInsertCli , Statement.RETURN_GENERATED_KEYS);
			rqt.setString(1, pClient.getNomClient());
			rqt.setString(2, pClient.getPrenomClient());
			rqt.setString(3,pClient.getEmailClient());
			rqt.setString(4, pClient.getAdresseClient());
			rqt.setString(5, pClient.getCodePostalClient());
			rqt.setString(6, pClient.getVille());
			
			int nbRows = rqt.executeUpdate();
			if(nbRows == 1){
				ResultSet rs = rqt.getGeneratedKeys();
				if(rs.next()){
					pClient.setIdClient(nbRows);
				}
			}
		} catch (SQLException e) {
			throw new DALException("Insert Reservation failed - " +pClient.getIdClient(), e);
		}finally {
			try {
				if (connect != null) {
					connect.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void insertSpec(Spectacle pSpectacle) throws DALException {
		Connection connect = null;
		PreparedStatement rqt = null;
		try {
			connect = JdbcTools.getConnection();
			rqt = connect .prepareStatement(sqlInsertSpec , Statement.RETURN_GENERATED_KEYS);
			rqt.setString(1, pSpectacle.getTitre());
			rqt.setString(2, pSpectacle.getArtiste());
			rqt.setString(3, pSpectacle.getLieu());
			rqt.setDate(4, (Date) pSpectacle.getDate());
			rqt.setInt(5, pSpectacle.getPlaces_disponibles());
			
			int nbRows = rqt.executeUpdate();
			if(nbRows == 1){
				ResultSet rs = rqt.getGeneratedKeys();
				if(rs.next()){
					pSpectacle.setIdspectacle(nbRows);
				}
			}
		} catch (SQLException e) {
			throw new DALException("Insert Reservation failed - " +pSpectacle.getIdspectacle(), e);
		}finally {
			try {
				if (connect != null) {
					connect.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(String code_reservation) throws DALException {
		PreparedStatement rqt = null;
		Connection connect = null;
		try {
			connect = JdbcTools.getConnection();
			rqt = connect.prepareStatement(sqlDelete);
			rqt.setString(1, code_reservation);
			rqt.executeUpdate();
		} catch (SQLException e) {
			throw new DALException("Delete Reservation failed - code reservation =" + code_reservation, e);
		} finally {
			try {
				if (rqt != null) {
					rqt.close();
				}
			} catch (SQLException e) {
				throw new DALException("close failed ", e);
			}
		}
	}

	@Override
	public List<Client> selectByClient() throws DALException {
		Connection connect = null;
		PreparedStatement rqt = null;
		ResultSet result = null;
		Client cli = null;
		List<Client> liste = new ArrayList<Client>();
		try {
			connect = JdbcTools.getConnection();
			rqt = connect.prepareStatement(sqlSelectByclient);
			result = rqt.executeQuery();
			while (result.next()) {
				cli =	new Client(result.getString("nomClient"), result.getString("prenomClient"),
								result.getString("emailClient"), result.getString("adresseClient"),
								result.getString("codePostal"), result.getString("villeClient"));
				cli.setIdClient(result.getInt("idClient"));
				liste.add(cli);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (rqt != null) {
					rqt.close();
				}
				if (connect != null) {
					connect.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			return liste;
	}

	@Override
	public List<Spectacle> selectBySpectacle() throws DALException {
		Connection connect = null;
		PreparedStatement rqt = null;
		ResultSet result = null;
		Spectacle spec = null;
		List<Spectacle> liste = new ArrayList<Spectacle>();
		try {
			connect = JdbcTools.getConnection();
			rqt = connect.prepareStatement(sqlSelectByspectacle);
			result = rqt.executeQuery();
			while (result.next()) {
				spec = new Spectacle(result.getString("titreS"), result.getString("artisteS"),
								result.getString("lieuS"), result.getDate("dateSpec"), result.getInt("placesDispo"));
				spec.setIdspectacle(result.getInt("idspectacle"));
				liste.add(spec);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (rqt != null) {
					rqt.close();
				}
				if (connect != null) {
					connect.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			return liste;
	}
	
	@Override
	public Client selectByIdClient(int id) throws DALException {
		Connection connect = null;
		PreparedStatement rqt = null;
		ResultSet result = null;
		Client cli = null;
		
		try {
			connect = JdbcTools.getConnection();
			rqt = connect.prepareStatement(sqlSelectByIdclient);
			rqt.setInt(1, id);
			result = rqt.executeQuery();
			if(result.next()){
				cli =	new Client(result.getString("nomClient"), result.getString("prenomClient"),
									result.getString("emailClient"), result.getString("adresseClient"),
									result.getString("codePostal"), result.getString("villeClient"));
				cli.setIdClient(result.getInt("idClient"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (rqt != null) {
					rqt.close();
				}
				if (connect != null) {
					connect.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			return cli;
	}
	
	@Override
	public Spectacle selectByIdSpectacle(int id) throws DALException {
		Connection connect = null;
		PreparedStatement rqt = null;
		ResultSet result = null;
		Spectacle spec = null;
		
		try {
			connect = JdbcTools.getConnection();
			rqt = connect.prepareStatement(sqlSelectByIdspectacle);
			rqt.setInt(1, id);
			result = rqt.executeQuery();
			if (result.next()) {
				spec = new Spectacle(result.getString("titreS"), result.getString("artisteS"),
								result.getString("lieuS"), result.getDate("dateSpec"), result.getInt("placesDispo"));
				spec.setIdspectacle(result.getInt("idspectacle"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (rqt != null) {
					rqt.close();
				}
				if (connect != null) {
					connect.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			return spec;
	}

}
