package fr.eni.spectacle.bo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Reservations {
	
	private String codeReservation;
	private Spectacle spec;
	private Client client;
	private int nb_places;
	private Date dateReservation;
	private int nbReserv;
	
	public Reservations( Spectacle spec, Client client, int nb_places,
			Date dateReservation) {
		nbReserv++;
		generatecodeReservation();
		setSpec(spec);
		setClient(client);
		this.nb_places = nb_places;
		this.dateReservation = dateReservation;
	}
	public String getCodeReservation() {
		return codeReservation;
	}
	public void setCodeReservation(String codeReservation) {
		this.codeReservation = codeReservation;
	}
	public Spectacle getSpec() {
		return spec;
	}
	public void setSpec(Spectacle spec) {
		this.spec = spec;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public int getNb_places() {
		return nb_places;
	}
	public void setNb_placess(int nb_places) {
		this.nb_places = nb_places;
	}
	public Date getDateReservation() {
		return dateReservation;
	}
	public void setDateReservation(Date dateReservation) {
		this.dateReservation = dateReservation;
	}
	
	public String generatecodeReservation() {
		codeReservation = "Reservati" + getDateAcutelle() + "" + nbReserv;
		return codeReservation;
	}
	public String getDateAcutelle() {
		Date actuelle = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String dat = dateFormat.format(actuelle);
		return dat;
	}
	
	@Override
	public String toString() {
		return "reservations [codeReservation=" + codeReservation + ", spec=" + spec + ", client=" + client
				+ ", nb_places=" + nb_places + ", dateReservation=" + dateReservation + "]";
	}

	
}
