package fr.eni.spectacle.bo;

import java.util.Date;

public class Spectacle {

	private int idspectacle;
	private String Titre;
	private String artiste;
	private String lieu;
	private Date date;
	private int places_disponibles;

	public Spectacle(int idspectacle, String titre, String artiste, String lieu, Date date, int places_disponibles) {
		super();
		this.idspectacle = idspectacle;
		Titre = titre;
		this.artiste = artiste;
		this.lieu = lieu;
		this.date = date;
		this.places_disponibles = places_disponibles;
	}

	public Spectacle(String titre, String artiste, String lieu, Date date, int places_disponibles) {
		super();
		Titre = titre;
		this.artiste = artiste;
		this.lieu = lieu;
		this.date = date;
		this.places_disponibles = places_disponibles;
	}

	public int getIdspectacle() {
		return idspectacle;
	}

	public void setIdspectacle(int idspectacle) {
		this.idspectacle = idspectacle;
	}

	public String getTitre() {
		return Titre;
	}

	public void setTitre(String titre) {
		Titre = titre;
	}

	public String getArtiste() {
		return artiste;
	}

	public void setArtiste(String artiste) {
		this.artiste = artiste;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getPlaces_disponibles() {
		return places_disponibles;
	}

	public void setPlaces_disponibles(int places_disponibles) {
		this.places_disponibles = places_disponibles;
	}

	@Override
	public String toString() {
		return "Spectacle [idspectacle=" + idspectacle + ", Titre=" + Titre + ", artiste=" + artiste + ", lieu=" + lieu
				+ ", date=" + date + ", places_disponibles=" + places_disponibles + "]";
	}

}
