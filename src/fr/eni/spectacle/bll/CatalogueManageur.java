package fr.eni.spectacle.bll;

import java.util.List;

import fr.eni.spectacle.bo.Client;
import fr.eni.spectacle.bo.Reservations;
import fr.eni.spectacle.bo.Spectacle;
import fr.eni.spectacle.dal.DALException;
import fr.eni.spectacle.dal.DAOFactory;
import fr.eni.spectacle.dal.SpectacleDAO;

public class CatalogueManageur {

	private static SpectacleDAO spectacleDAO;

	public CatalogueManageur() {
		spectacleDAO = DAOFactory.getSpectacleDAO();
	}

	public List<Reservations> getCatalogue() throws BLLException {
		List<Reservations> liste = null;
		try {
			liste = spectacleDAO.selectAll();
		} catch (DALException e) {
			e.printStackTrace();
			throw new BLLException("Erreur récupération catalogue", e);
		}
		return liste;
	}
	
	public List<Spectacle> getSpectacle() throws BLLException {
		List<Spectacle> liste = null;
		try {
			liste = spectacleDAO.selectBySpectacle();
		} catch (DALException e) {
			e.printStackTrace();
			throw new BLLException("Erreur récupération catalogue", e);
		}
		return liste;
	}
	
	public List<Client> getClient() throws BLLException {
		List<Client> liste = null;
		try {
			liste = spectacleDAO.selectByClient();
		} catch (DALException e) {
			e.printStackTrace();
			throw new BLLException("Erreur récupération catalogue", e);
		}
		return liste;
	}

	public void validerReservation(Reservations res) throws BLLException {
		boolean valider = true;
		StringBuffer sb = new StringBuffer();

		if (res == null) {
			throw new BLLException("Reservation nulle");
		}

		if (res.getCodeReservation() == null || res.getCodeReservation().trim().length() == 0) {
			valider = false;
			sb.append("Le code reservation est obligatoire !");
		}

		if (res.getDateReservation() == null) {
			valider = false;
			sb.append("La date de reservation est obligatoire ! ");
		}

		if (res.getNb_places() == 0) {
			valider = false;
			sb.append("Le nombre de place est obligatoire !");
		}

		if (res.getClient() == null) {
			valider = false;
			sb.append("Le client est obligatoire !");
		}

		if (res.getSpec() == null) {
			valider = false;
			sb.append("Il n'y a pas de spectacle de rengister !");
		}
		
		if(!valider){
			throw new BLLException(sb.toString());
		}
	}

	public void addReservation(Reservations res) throws BLLException {
		if(res.getCodeReservation() != null ) {
			throw new BLLException("Reservation Existante !");
		}
		try {
			validerReservation(res);
			spectacleDAO.insertReserv(res);
		} catch (DALException e) {
			throw new BLLException("Echec d'insert de l'article !", e);
		}
	}
	
	public void addClient(Client cli) throws BLLException {
		try {
			spectacleDAO.insertCli(cli);
		} catch (DALException e) {
			throw new BLLException("Echec d'insert de l'article !", e);
		}
	}
	
	public void addSpectacle(Spectacle spec) throws BLLException {
		try {
			spectacleDAO.insertSpec(spec);
		} catch (DALException e) {
			throw new BLLException("Echec d'insert de l'article !", e);
		}
	}

	public void removeReservation(Reservations res) throws BLLException {
		try {
			spectacleDAO.delete(res.getCodeReservation());
		} catch (DALException e) {
			throw new BLLException("Echec delete Reservation", e);
		}
	}

	public void updateReservation(Reservations res ) throws BLLException {
		try {
			validerReservation(res);
			spectacleDAO.update(res);
		} catch (DALException e) {
			throw new BLLException("Echec de l'update ", e);
		}
	}
	
	public void selectByIdClient(int id) throws BLLException {
		try{
			spectacleDAO.selectByIdClient(id);
		} catch (Exception e) {
			throw new BLLException("Echec de du selectByIdClient ", e);
	}
	
	public void selectByIdSpectacle(int id) throws BLLException {
		try{
			spectacleDAO.selectByIdSpectacle(id);
		} catch (Exception e) {
			throw new BLLException("Echec du selectByIdSpectacle ", e);
	}}
}
