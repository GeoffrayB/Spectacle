insert into spectacle
values('good by tour', 'johnny halliday', 'zenith de nantes', '2018-02-01', 200);
insert into spectacle
values('facebook', 'les vamps', 'zenith de paris', '2019-02-01', 500);

insert into client
values('coignat', 'guillaume', 'guillaume.coignat@gmail.com', '79, rue de la gaudinière', '44100', 'Nantes');
insert into client
values('bigot', 'goeffray', 'geoffray.bigot@gmail.com', '1, rue de quelque part', '44000', 'Nantes');

insert into reservation
values('a1z2e3r4t5y6u7i8o9p0', 1, 1, 2, CURRENT_TIMESTAMP);
insert into reservation
values('0p9o2i3u4y5t6r7e8z9a', 2, 2, 4, CURRENT_TIMESTAMP);