use SPECTACLE_DB;

DROP TABLE reservation;
DROP TABLE client;
DROP TABLE spectacle;

create table spectacle (
	id int not null IDENTITY(1,1),
	titre varchar(200),
	artiste varchar(200),
	lieu varchar(200),
	date_spec DATE,
	places_disponibles int 
);

alter table spectacle
ADD CONSTRAINT id_spectacle_pk PRIMARY KEY (id) ;

create table client(
	id int not null IDENTITY(1,1),
	nom varchar(200),
	prenom varchar(200),
	email varchar(200),
	adresse varchar(200),
	code_postal char(5),
	ville varchar(200)
);

Alter table client
ADD CONSTRAINT id_client_pk PRIMARY KEY(id);

create table reservation (
	code_reservation char(20) not null,
	spectacle_id int not null,
	client_id int not null,
	nombre_places int,
	date_reservation DATETIME
);

Alter table reservation
ADD CONSTRAINT code_reservation_pk PRIMARY KEY (code_reservation);

ALTER TABLE reservation
ADD CONSTRAINT spectacle_id_fk FOREIGN KEY (spectacle_id) REFERENCES spectacle(id);
ALTER TABLE reservation
ADD CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES client(id);